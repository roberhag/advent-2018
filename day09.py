#day 9 a
from numpy import *
from collections import deque
inp = "465 players; last marble is worth 71498 points"

np = 465  #num players
mm = 714980 #max marble
circ = deque([0], mm) #works in py 3
i = 0   #current index
scores = zeros(np + 1, dtype=int) #index zero not used
for m in range(1, mm+1):
    if m % 50000 == 0:
        print(m)
    if m % 23 == 0:
        p = ((m - 1) % np) + 1
        i = (i - 7) % len(circ)
        pts = m + circ[i]
        del circ[i]
        scores[p] += pts
        #print("player %i got %i points." % (p, pts))
        continue
    i = ((i + 1) % len(circ)) + 1
    circ.insert(i, m)
    #print(circ)
print(max(scores))    
