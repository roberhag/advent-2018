#day 20 ... I tried to do it recursively but the recursion depth is reached.
from numpy import *
from collections import deque
import sys
N = 200
instr = open("inp20.txt").read().strip("\n")
#instr = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"
#instr = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"
instr = instr[1:-1] #remove beginning and end symbols
rooms = zeros((N, N), dtype="uint8") #Using 4 bits to store which doors exist, 
#8 bit unsigned int is one byte in memory (but 4 bits too many).
#each room is 0 if no doors, 1 if north, 2 if south, 4 if east, 8 is west. Can be summed.
#North x&1, South x&2, E x&4, W x&8
doornum = {"N": 1, "S": 2, "E":4, "W":8} # This room should have this bit active,
othernum = {"N": 2, "S": 1, "E":8, "W":4}# Next room also has the same/corresponding door
dxd = {"N": 0, "S": 0, "E":1, "W":-1}
dyd = {"N": -1, "S": 1, "E":0, "W":0}
sys.setrecursionlimit(4000)
#print(sys.getrecursionlimit())
onedic = {}
for m in "NSEW": #making one dict to just look up once per iteration
    onedic[m] = (dxd[m], dyd[m], doornum[m], othernum[m])

def move_simple(x,y,k,kmax):
    if k >= kmax:
        return (x, y)
    if (x<0) or (y<0) or (x>=N) or (y>=N):
        print("increase N")
        return
    for k in range(k, kmax):
        m = instr[k]
        dx, dy, dn, on = onedic[m]
        rooms[x,y] |= dn
        x += dx
        y += dy
        rooms[x,y] |= on
    return (x, y)

def move_recursive(x, y, k, kmax):
    if k >= kmax:
        return [(x,y)]
    if "|" in instr[k:kmax]: #this part of the regex branches
        #p1, p2 = regex.split("|", 1) #These splits are memory inefficient!
        opar = instr[k:kmax].find("(") + k
        pipe = instr[k:kmax].find("|") + k
        #TODO keep the string unmodified in memory, just pass indices.
        if opar < pipe: #we have some common path before branch
            #p1, p2 = regex.split("(", 1) #p1 contains string common for all paths
            d = 1 #count the depth of parentheses
            i = opar + 1
            while d > 0:
                if instr[i] == "(":
                    d += 1
                elif instr[i] == ")":
                    d -= 1
                elif d == 1:
                    if instr[i] == "|":
                        pipe = i #should find exactly one split on level 1
                i += 1
            cpar = i - 1
            #print (instr[k:opar], instr[opar + 1:pipe], instr[pipe+1:cpar], instr[cpar + 1:kmax])
            if opar > k:
                x, y = move_simple(x, y, k, opar)
                
            branches = move_recursive(x, y, opar + 1, pipe)
            branches += move_recursive(x, y, pipe + 1, cpar)
            returns = []
            for c in branches:
                returns += move_recursive(*c, cpar + 1, kmax)
            return returns
        else: #branch happens before next parenthesis
            branches = move_recursive(x, y, k, pipe)
            branches += move_recursive(x, y, pipe + 1, kmax)
            return branches
    else:
        return [move_simple(x, y, k, kmax)]
        

move_recursive(N//2, N//2, 0, len(instr))

def printrooms():
    for r in rooms.transpose():
        if (r == 0).all(): continue
        i = 0
        while r[i] == 0:
            i += 1
        while r[i] != 0:
            print("%2i" % r[i], end="")
            i += 1
        print()

    #each room is 0 if no doors, 1 if north, 2 if south, 4 if east, 8 is west.
possi = []
for i in range(16): #rooms can be numbered 0 to 15 with the 4 door direction bits
    p = []
    if i & 1:
        p.append((0,-1))
    if i & 2:
        p.append((0,1))
    if i & 4:
        p.append((1,0))
    if i & 8:
        p.append((-1,0))
    possi.append(p)
    
def find_dists():
    dists = zeros((N, N), dtype="uint64")
    dists[:,:] = N*N
    tocheck = deque([(N//2, N//2, 0)])
    while len(tocheck) > 0:
        x, y, d = tocheck.popleft()
        #print(x, y, d)
        if dists[x,y] <= d:
            #print("cnt")
            continue #This coordinate already has a better way.
        dists[x,y] = d
        drs = rooms[x,y]
        for p in possi[drs]:
            dx, dy = p
            tocheck.append((x+dx, y+dy, d+1))
    mx = max(dists[dists < N*N])
    print(mx)
    return mx
find_dists()
